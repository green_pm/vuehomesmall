/*jshint esversion: 6 */

import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import VueRouter from 'vue-router';

import body    from '../body';
import about   from '../about';
import login   from '../login';
import camera  from '../camera';
//import yaMusic  from '../yaMusic';
import roles   from '../roles-editor';
import folders from '../folders-editor';

Vue.use(Vuex);
Vue.use(VueRouter);
const router = new VueRouter({
  routes: [
    { path: '/', component: about },
    { path: '/fotos/:objType?/:objId?', component: body, params: true },
    { path: '/roles', component: roles },
    { path: '/login', component: login },
    { path: '/camera', component: camera },
    { path: '/folders', component: folders },
  ]
});

router.isIni = false;
router.afterEach((to, from) => {
    if (to.path.substr(0, 6) == "/fotos" && store.state.user.isAuth === true) {
        if (!store.state.router.isIni &&
            (!to.params.objId || to.params.objId.length < 1) &&
            (from.params.objId && from.params.objId.length > 0)) {
            store.state.router.isIni = true;
            store.state.router.push({ path: `/fotos/folder/${from.params.objId}` });
        }
        else store.dispatch('loadFiles', to.params.objId ? to.params.objId : '');
    }
});

const store = new Vuex.Store({
    state: {
        router,
        user: {
            isAuth: false,
            isWait: false,
            isAdmin: false,
            login: '',
            token: '',
            roles: [],
        },
        //apiUrl: process.env.node_env === 'production' ? 'https://api.grekhov.dev' : 'http://localhost:5000',
        //apiUrl: 'https://api.grekhov.dev',
        //apiUrl: 'http://grekhov.dyndns.org:88',
        //apiUrl: 'https://localhost:5001',
        apiUrl: 'http://localhost:5000',
        list: [],
        selDir: {
            name: '',
            fullName:'',
            icon:'',
        },
        selFile: {
            name: '',
            fullName:'',
            icon:'',
            size: 0,
            type: '',
        },
        size: {
            arr: [32, 50, 100, 150, 200], // 300, 450, 600, 900],
            sel: 100
        },
        count: {
            arr: [6, 12, 18, 24, 30, 36, 42, 48],
            sel: 12
        },
        sorting: {
            arr: [
                { text: 'A >> Z', value: 'asc' },
                { text: 'Z >> A', value: 'desc' }
            ],
            sel: 'asc',
            OrderBy: function (a, b, field, asc) {
                var result = 0;
                if (a[field] > b[field]) result = 1;
                else if (a[field] < b[field]) result = -1;
                result *= (asc === true ? 1 : -1);
                return result;
            },
        },
        currPage: {
            elFirst: 0,
            elLast: 0
        },
        cbCompress: false,
        infoList: [],
        infoListTimer: 20, // second // 
        strings: {
            login: {
                login:    'Вход',
                registr:  'Регистрация',
                email:    'Ваша электронная почта',
                password: 'Ваш пароль для входа на сайт',
                confirm:  'Повторите Ваш пароль'
            }
        },
        formats: {
            sel: 'all',
            arr: [
                { text: 'Все',      value: 'all'   },
                { text: 'Видео',    value: 'video' },
                { text: 'Аудио',    value: 'audio' },
                { text: 'Картинки', value: 'image' },
            ],
            video: [
                'avi',
                'mov',
                'mp4',
                'mkv',
            ],
            audio: [
                'mp3',
                'wav'
            ],
            image: [
                'jpg',
                'jpeg',
                'png',
                'ico',
                'bmp',
                'gif',
            ],
        },
    },
    getters: {
        getFilteredList(state) {
            let buff = state.list;
            let list = [];
            for (const el of buff)
                if (el.isFolder)
                    list.push(el);
            if (state.formats.sel != 'all')
                buff = buff.filter(el => state.formats[state.formats.sel].indexOf(el.extension.toLowerCase()) > -1);
            else buff = buff.filter(el => !el.isFolder);
            buff = buff.sort((a, b) => state.sorting.OrderBy(a, b, 'name', state.sorting.sel == 'asc'));
            for (const el of buff)
                list.push(el);
            return list;
        },
    },
    mutations: {
        setParam(state, {type, items}) {
            state[type] = items;
        },
        setParam2(state, {type, type2, value}) {
            state[type][type2] = value;
            if (type == 'size' && type2 == 'sel')
                window.localStorage.setItem('size.sel', value);
            if (type == 'count' && type2 == 'sel')
                window.localStorage.setItem('count.sel', value);
        },
        setList(state, {type, items}) {
            Vue.set(state, type, items);
        },
        setAuth(state, token) {
            state.user.isAdmin = false;
            state.user.isAuth = (token && token.length > 0);
            state.user.token = token;
            window.localStorage.setItem('token', token);
            if (state.user.isAuth) {
                if (state.router.history.current.path == '/login')
                    state.router.push('/fotos');
                var arr = token.split('.');
                var user = JSON.parse(atob(arr[1]));

                this.commit('setUser', user.sub);
                state.user.roles = [];
                for (var el in user) {
                    if (el.indexOf('role') > -1) {
                        state.user.roles = user[el];
                        if (user[el] == 'Admin')
                            state.user.isAdmin = true;
                        if (user[el].indexOf('Admin') > -1)
                            state.user.isAdmin = true;
                        break;
                    }
                }
            }
            else this.commit('setUser', '');
        },
        setUser(state, login) {
            state.user.login = login;
            window.localStorage.setItem('login', login);
            if (!login)
                state.router.push('/');
        },
        setInfo(state, info) {
            var el = {};
            el.status = info.status ? info.status :
                        info.response && info.response.status ? info.response.status : '∞';
            el.error = el.status == '∞' || el.status > 299 || (info.data && info.data.type != 1);
            el.id = Date.now().toLocaleString();
            el.timermax = el.timer = state.infoListTimer;

            if (el.status == 401) {
                el.statusText = 'Error';
                el.message = 'Вы не авторизованы, либо срок действия ключа истёк.';
                el.comment = 'Необходимо войти в свой аккаунт.';
                this.commit('setAuth', '');
            } else if (el.error) {
                if (info.response) {
                    el.statusText = info.response.statusText ? info.response.statusText : 'Error';
                    el.message = info.response.data ? info.response.data.message : info.message;
                    el.comment = info.response.data ? info.response.data.comment : info.stack;
                } else if (info.data && info.data.type != 1) {
                    el.status = '∞';
                    el.statusText = 'Error';
                    el.message = info.data.message ? info.data.message : info.message;
                    el.comment = info.data.comment ? info.data.comment : info.stack;
                }
            } else {
                el.statusText = info.statusText ? info.statusText : 'Ok';
                el.message = info.data && info.data.message ? info.data.message : '';
                el.comment = info.data && info.data.comment ? info.data.comment : '';
            }
            state.infoList.push(el);
            
            var timer = (el) => { setTimeout(function (el) { el.timer--; if (el.timer > 0) timer(el); }, 1000, el); };
            timer(el);

            setTimeout(function () {state.infoList.splice(0, 1);}, state.infoListTimer * 1000);
        },
    },
    actions: {
        logout() {
            this.commit('setAuth', '');
        },
        isAuth(context) {
            this.dispatch('pageIni');
            var requestUrl = this.state.apiUrl + '/account/isAuth';
            var token = window.localStorage.getItem('token');
            if (token && token.length > 0) {
                this.commit('setParam2', {type: 'user', type2: 'isWait', value: true});
                axios({
                    method: 'GET',
                    url: requestUrl,
                    headers: {
                        'authorization': 'Bearer ' + token,
                        'content-type': 'application/json',
                        'cache-control': 'no-cache'
                    },
                })
                .then(response => {
                    this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                    if (response.data == true)
                         this.commit('setAuth', token);
                    else this.commit('setAuth', '');
                })
                .catch(err => {
                    this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                    this.commit('setInfo', err);
                    this.commit('setAuth', '');
                });
            }
            else this.commit('setAuth', '');
        },
        login(context, user) {
            var requestUrl = this.state.apiUrl + '/Account/Login';

            this.commit('setParam2', {type: 'user', type2: 'isWait', value: true});
            axios.post(requestUrl, user, {
                //withCredentials: true,
                headers: {
                    //'Access-Control-Allow-Origin': '*',
                    //'accept': 'application/json',
                    'content-type': 'application/json',
                    'cache-control': 'no-cache'
                  },
            })
            .then((response) => {
                this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                this.commit('setInfo', response);
                if (response.data && response.data.type == 1) {
                    this.commit('setAuth', response.data.data);
                    this.dispatch('loadFiles', '');
                }
            })
            .catch(err => {
                this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                this.commit('setInfo', err);
                console.log(err);
            });
        },
        registr(context, user) {
            var requestUrl = this.state.apiUrl + '/account/register';

            this.commit('setParam2', {type: 'user', type2: 'isWait', value: true});
            axios({
                method: 'POST',
                url: requestUrl,
                data: user,
                headers: {
                    'content-type': 'application/json',
                    'cache-control': 'no-cache'
                  },
            })
            .then((response) => {
                this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                this.commit('setInfo', response);
                // if (response.data && response.data.type == 1)
                //      alert(response.data.message + " " + response.data.comment);
                // else alert(response.data.message + " " + response.data.comment);
            })
            .catch(err => {
                this.commit('setParam2', {type: 'user', type2: 'isWait', value: false});
                this.commit('setInfo', err);
                console.log(err);
            });
        },
        pageIni() {
            var val = 0;
            val = window.localStorage.getItem('size.sel');
            if (val) this.commit('setParam2', {type: 'size', type2: 'sel', value: val});
            val = window.localStorage.getItem('count.sel');
            if (val) this.commit('setParam2', {type: 'count', type2: 'sel', value: val});
        },
        loadFiles(context, dir) {
            this.commit('setParam2', {type: 'selDir', type2: 'fullName', value: dir});
            this.commit('setList', {type: 'list', items: []});
            var requestUrl = this.state.apiUrl + '/Files/GetFiles';
            if (!dir || dir.length < 1)
                dir = '';
            axios({
                method: 'POST',
                url: requestUrl,
                data: { name: dir },
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'content-type': 'application/json',
                    'cache-control': 'no-cache'
                  },
            })
            .then((response) => {
                response.data.data.forEach(el => {
                    if (!el.name || el.name.length < 1)
                        el.name = '.' + el.extension;
                });
                this.commit('setList', {type: 'list', items: response.data.data});
            })
            .catch(err => {
                this.commit('setInfo', err);
                console.log(err);
            });
        },
        loadIcons(context, params) {
            var first = params.first;
            var end = params.end;
            var size = params.size;

            if (first < 0) first = this.state.currPage.elFirst;
            else this.commit('setParam2', {type: 'currPage', type2: 'elFirst', value: first});
            if (end   < 0) end   = this.state.currPage.elLast;
            else this.commit('setParam2', {type: 'currPage', type2: 'elLast', value: end});
            if (!size || size < 16) size = this.state.size.sel;
            else this.commit('setParam2', {type: 'size', type2: 'sel', value: size});

            this.dispatch('loadIconsPart');
        },
        loadIconsPart(context) {
            var max = this.state.count.sel <=  24 ? 1 : 2;
            for (var i = this.state.currPage.elFirst; i < this.state.currPage.elLast; i += max)
                if (!this.getters.getFilteredList[i].iconSize ||
                    this.getters.getFilteredList[i].iconSize != this.state.size.sel) {
                    var icoUrl = this.state.apiUrl + '/Files/GetIcons';
                    var data = {
                        names: this.getters.getFilteredList.slice(i, i + max).map( el => el.fullName),
                        size: this.state.size.sel,
                        compress: this.state.cbCompress
                    };
                    axios({
                        method: 'POST',
                        url: icoUrl,
                        data: data,
                        headers: {
                            'authorization': 'Bearer ' + this.state.user.token,
                            'content-type': 'application/json',
                            'cache-control': 'no-cache'
                        },
                    })
                    .then((response) => {
                        var list = this.getters.getFilteredList;
                        var localfirst = 0;
                        for (var i = 0; i < response.data.data.length; i++) {
                            if (i + localfirst >= list.length)
                                return;
                            if (list[i + localfirst].fullName == response.data.data[i].name) {
                                list[i + localfirst].iconSize = this.state.size.sel;
                                list[i + localfirst].icon = this.state.cbCompress ?
                                    zip.LZW.decompress(response.data.data[i].data) :
                                    response.data.data[i].data;
                            }
                            else {
                                var markFind = false;
                                for (var j = 0; j < list.length; j++)
                                    if (list[j].fullName == response.data.data[i].name) {
                                        localfirst = j;
                                        list[i + localfirst].iconSize = this.state.size.sel;
                                        list[i + localfirst].icon = this.state.cbCompress ?
                                            zip.LZW.decompress(response.data.data[i].data) :
                                            response.data.data[i].data;
                                        markFind = true;
                                        break;
                                    }
                                
                                if (!markFind)
                                    return;
                            }
                        }
                        this.dispatch('loadIconsPart');
                    })
                    .catch(err => {
                        this.commit('setInfo', err);
                        console.log(err);
                    });

                    return;
                }
        },
        loadImg(context, fileName) {
            var icoUrl = this.state.apiUrl + '/Files/GetIcons';
            var data = {
                names: [fileName],
                size: 1000,
                compress: false
            };
            axios({
                method: 'POST',
                url: icoUrl,
                data: data,
                headers: {
                    'authorization': 'Bearer ' + this.state.user.token,
                    'content-type': 'application/json',
                    'cache-control': 'no-cache'
                  },
            })
            .then((response) => {
                this.commit('setParam2', {type: 'selFile', type2: 'icon', value: response.data[0]});
            })
            .catch(err => {
                this.commit('setInfo', err);
                console.log(err);
            });
        },
    },
});

export default store;