/*jshint esversion: 6 */

import Vue from 'vue';
import App from './App.vue';

import style from './content/style.css';
Vue.use(style);

// import VueMaterial from 'vue-material';
// import 'vue-material/dist/vue-material.min.css';
// Vue.use(VueMaterial);

// // import JavaScript
// import ElementUi from 'element-ui';
// // import CSS 
// import 'element-ui/lib/theme-chalk/index.css';
// Vue.use(ElementUi);

import BootstrapVue from 'bootstrap-vue';
Vue.use (BootstrapVue);
import  'bootstrap/dist/css/bootstrap.css';
import  'bootstrap-vue/dist/bootstrap-vue.css';


if ((location.host.indexOf('localhost') < 0 && location.protocol != 'https:') || location.host == 'xn--b1abg0bi5a7b.xn--p1ai/') {
  location.href = 'https://grekhov.dev';
  // 'https:' + window.location.href.substring(window.location.protocol.length);
} else {
  new Vue({
    el: '#app',
    render: h => h(App),
  });
}
